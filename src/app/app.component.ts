import {Component, computed, effect, signal} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent{
  title = 'sample-app';
  counter = 0;
  testSignal = signal('')
  constructor() {
    this.testSignal.set('Init state of root');
    const computedSignal = computed( () => this.testSignal() + ' computed')
    effect(() => console.log(computedSignal()))
  }

  onClick(){
    this.counter++;
    this.testSignal.set('modification: ' + this.counter);
  }
}
