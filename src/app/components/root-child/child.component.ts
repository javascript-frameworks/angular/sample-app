import {Component, signal} from '@angular/core';
import {NgIf} from "@angular/common";

@Component({
  selector: 'child-component',
  templateUrl: './child.component.html',
  imports: [
    NgIf
  ],
  standalone: true
})
export class ChildComponent{
  signalChild = signal('');

  onClick() {
    this.signalChild.set("Hello im signal");
  }
}
