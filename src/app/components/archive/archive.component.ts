import {Component, Input} from '@angular/core';
import {NgIf} from "@angular/common";


@Component({
  selector: 'archive-component',
  templateUrl: './archive.component.html',
  imports: [
    NgIf
  ],
  standalone: true
})
export class ArchiveComponent {
  @Input() archivedItemId?: string;
}
