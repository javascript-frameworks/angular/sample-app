import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ArchiveComponent} from "./components/archive/archive.component";

const routes: Routes = [
  {
    path: "archive",
    component: ArchiveComponent,
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      bindToComponentInputs: true
    }),
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
